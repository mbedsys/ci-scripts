#!/bin/bash -e

if [ $# -eq 0 ]; then
    echo "Usage $0 <archive file>"
    exit 1
fi

source /etc/os-release
: ${OS_ID:=$ID}
: ${OS_VERSION_CODENAME:=$VERSION_CODENAME}
printf '%s=%s\n' PACKAGE_OS_NAME "$OS_ID" PACKAGE_OS_VERSION "$OS_VERSION_CODENAME" > "$1.pkginfo"

case "$1" in
    *.deb)
        dpkg -I "$1" | awk '
            $1 == "Architecture:" {
                printf("PACKAGE_ARCH=%s\n", $2);
            }
            $1 == "Package:" {
                printf("PACKAGE_NAME=%s\n", $2);
            }
            $1 == "Version:" {
                printf("PACKAGE_VERSION=%s\n", $2);
            }
            END {
                print "PACKAGE_TYPE=deb"
            }
        ' >> "$1.pkginfo"
    ;;
esac

echo "File [$1.pkginfo] generated"
