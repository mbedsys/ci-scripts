#!/bin/bash -e

: ${SCRIPTS_DIR:=$(readlink -f "$(dirname "$0")")}
: ${REPO_ROOT_PATH:=$(git rev-parse --show-toplevel)}

if [ -e "$REPO_ROOT_PATH/.ci-toolbox.properties" ]; then
    source "$REPO_ROOT_PATH/.ci-toolbox.properties"
fi
: ${REPO_CI_SCRIPTS_DIR:="${CITBX_SCRIPTS_DIR:-"ci/scripts"}"}

mkdir -p "$REPO_ROOT_PATH/$REPO_CI_SCRIPTS_DIR"
ln -sfr "$SCRIPTS_DIR/common.sh" "$REPO_ROOT_PATH/$REPO_CI_SCRIPTS_DIR/common.sh"
ln -sfr "$SCRIPTS_DIR/run-job-script.sh" "$REPO_ROOT_PATH/$REPO_CI_SCRIPTS_DIR/run-job-script.sh"

FILE_LIST=($(find "$SCRIPTS_DIR" -mindepth 2 -type f -name "*.sh" | sed 's|^'"$SCRIPTS_DIR"/'||g' | sort -u))

while true; do
    if [ "$summary" != "false" ]; then
        echo
        echo "Available entry list:"
        for i in ${!FILE_LIST[@]}; do
            printf "* [%d]\t: %s\n" $i "${FILE_LIST[$i]}"
        done
    fi
    summary=true
    echo -n "Choose entry number or [Ctrl+C] to exit: "
    read entry_id
    test -n "$entry_id" && test -n "${FILE_LIST[$entry_id]}" || continue
    source_path="$SCRIPTS_DIR/${FILE_LIST[$entry_id]}"
    target_path_rel="$REPO_CI_SCRIPTS_DIR/${FILE_LIST[$entry_id]}"
    echo -n "Target location [$target_path_rel]: "
    read target_path_rel_new
    if [ -z "$target_path_rel_new" ]; then
        target_path="$REPO_ROOT_PATH/$target_path_rel"
    elif [ "${target_path_rel_new:0:1}" == '/' ]; then
        target_path="$REPO_ROOT_PATH/$target_path_rel_new"
    else
        target_path="$REPO_ROOT_PATH/$(dirname "$target_path_rel")/$target_path_rel_new"
    fi
    mkdir -p "$(dirname "$target_path")"
    echo "LINK [${source_path:${#REPO_ROOT_PATH}}]=>[${target_path:${#REPO_ROOT_PATH}}]"
    ln -sfr "$source_path" "$target_path"
    summary=false
done
