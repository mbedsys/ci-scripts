#!/bin/bash -e

: ${REPO_ROOT_PATH:=$(git rev-parse --show-toplevel)}

if [ -e "$REPO_ROOT_PATH/.ci-toolbox.properties" ]; then
    source "$REPO_ROOT_PATH/.ci-toolbox.properties"
fi
: ${REPO_CI_TEMPLATES_DIR:="ci/templates"}

mkdir -p "$REPO_ROOT_PATH/$REPO_CI_TEMPLATES_DIR"

print_usage() {
    echo "Usage: $0 install|update [args...]"
    exit 1
}

tmpl_install() {
    local tmpl_file_list=($(
        curl -X GET -s https://gitlab.com/api/v4/projects/mbedsys%2Fci-templates/repository/tree?ref=master \
            | jq -r '. | map(.name)[] | select(. | endswith(".yml"))'
    ))
    if [ $# -eq 0 ]; then
        echo "Available templates:"
        for i in ${!tmpl_file_list[@]}; do
            printf "* [%d]\t: %s\n" $i "${tmpl_file_list[$i]}"
        done
        echo "Usage: $0 install <entry no> [<entry no> [<entry no>...]]"
        return
    fi
    for i in "$@"; do
        local tmpl_name=${tmpl_file_list[$i]}
        if [ -z "$tmpl_name" ]; then
            echo "Invalid entry [$i]"
            return 1
        fi
        echo "INSTALL [$REPO_ROOT_PATH/$REPO_CI_TEMPLATES_DIR/$tmpl_name]"
        wget -O "$REPO_ROOT_PATH/$REPO_CI_TEMPLATES_DIR/$tmpl_name" \
            https://gitlab.com/api/v4/projects/mbedsys%2Fci-templates/repository/files/$tmpl_name/raw?ref=master
    done
}

tmpl_update() {
    for file in $(ls "$REPO_ROOT_PATH/$REPO_CI_TEMPLATES_DIR"); do
        echo "UPDATE [$REPO_ROOT_PATH/$REPO_CI_TEMPLATES_DIR/$file]"
        wget -O "$REPO_ROOT_PATH/$REPO_CI_TEMPLATES_DIR/$file" \
            https://gitlab.com/api/v4/projects/mbedsys%2Fci-templates/repository/files/$file/raw?ref=master || true
    done
}

main() {
    local cmd=$1
    shift || print_usage

    case "$cmd" in
        i|install)
            tmpl_install "$@"
        ;;
        u|update)
            tmpl_update
        ;;
        *)
            print_usage
        ;;
    esac
}

main "$@"
