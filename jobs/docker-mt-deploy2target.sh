citbx_use "multitarget" 
citbx_use "dind"

job_define() {
    bashopts_declare -n TARGET_HOSTS -l target-host-list \
        -d "Target host list ('host1 host2 etc.')" -t string -r -s
    bashopts_declare -n OS_SSH_RSA_PRIVATE_KEY -l ssh-rsa-private-key \
        -d "Target SSH private key to connect to the hosts" -t string -s
    bashopts_declare -n OS_SSH_DSA_PRIVATE_KEY -l ssh-dsa-private-key \
        -d "Target SSH private key to connect to the hosts" -t string -s
    bashopts_declare -n OS_SSH_ECDSA_PRIVATE_KEY -l ssh-ecdsa-private-key \
        -d "Target SSH private key to connect to the hosts" -t string -s
    bashopts_declare -n OS_SSH_ED25519_PRIVATE_KEY -l ssh-ed25519-private-key \
        -d "Target SSH private key to connect to the hosts" -t string -s
    citbx_export TARGET_HOSTS OS_SSH_RSA_PRIVATE_KEY \
        OS_SSH_DSA_PRIVATE_KEY OS_SSH_ECDSA_PRIVATE_KEY \
        OS_SSH_ED25519_PRIVATE_KEY
}

job_setup() {
    if [ -z "$OS_SSH_RSA_PRIVATE_KEY" ] \
        && [ -z "$OS_SSH_DSA_PRIVATE_KEY" ] \
        && [ -z "$OS_SSH_ECDSA_PRIVATE_KEY" ] \
        && [ -z "$OS_SSH_ED25519_PRIVATE_KEY" ]; then
        for type in rsa dsa ecdsa ed25519; do
            if [ -f $HOME/.ssh/id_$type ]; then
                citbx_docker_run_add_args -v "$HOME/.ssh/id_$type:$HOME/.ssh/id_$type" \
                    -v "$HOME/.ssh/id_$type:/root/.ssh/id_$type"
            fi
        done
    fi
}

job_main() {
    local target_host
    local target_user=${TARGET_HOST_USER:-root}
    if [ -z "$TARGET_HOSTS" ]; then
        print_critical "Undeclared TARGET_HOSTS. This environment variable must contains the target host list"
    fi
    mkdir -p $HOME/.ssh
    sudo chown $(id -u):$(id -g) $HOME/.ssh
    chmod 700 $HOME/.ssh
    for type in rsa dsa ecdsa ed25519; do
        if eval "[ -n \"\$OS_SSH_${type^^}_PRIVATE_KEY\" ]"; then
            eval "echo -e \"\$OS_SSH_${type^^}_PRIVATE_KEY\" > $HOME/.ssh/id_$type"
            chmod 600 $HOME/.ssh/id_$type
        fi
    done
    if [ $(find $HOME/.ssh/ -regex '.*/id_\(rsa\|dsa\|ecdsa\|ed25519\)' | wc -l) -eq 0 ]; then
        print_critical "No SSH private key found into $HOME/.ssh" \
            "You may have to define one of OS_SSH_(RSA|DSA|ECDSA|ED25519)_PRIVATE_KEY environment variable"
    fi
    if [ "$DOCKER_HOST" != "unix:///var/run/docker.sock" ]; then
        # Login only if runned in DIND mode (e.g. on a Gitlab runner and not on a workstation)
        docker login --username gitlab-ci-token --password-stdin "$CI_REGISTRY" <<< "$CI_JOB_TOKEN"
        docker pull $TARGET_CI_REGISTRY_IMAGE_TAG
    fi
    for target_host in $TARGET_HOSTS; do
        print_info "Deploying onto target $target_host..."
        ssh-keygen -R $target_host > /dev/null 2>&1 || true
        ssh-keyscan -H $target_host 2>/dev/null >> $HOME/.ssh/known_hosts \
            || print_critical "$target_host target not reachable"
        docker image tag $TARGET_CI_REGISTRY_IMAGE_TAG $TARGET_NAME:$TARGET_VERSION
        docker image save $TARGET_NAME:$TARGET_VERSION | ssh $target_user@$target_host docker load
        sed '
            s|\${DOCKER_IMAGE}|'"$TARGET_NAME:$TARGET_VERSION"'|g;
            s|\${APP_NAME}|'"$TARGET_NAME"'|g;
            s|\${DEPLOY_TIMESTAMP}|'"$(date +'%F %T')"'|g;
            ' targets/$TARGET_NAME/docker-compose.yml \
                | ssh $target_user@$target_host "cat > /media/data/services/docker-compose.${DOCKER_COMPOSE_LOAD_ORDER:-50}-$TARGET_NAME.yml"
    done
}
