citbx_use "multitarget"
citbx_use "dind"

job_setup() {
    TARGET_DOCKER_BUILD_FORCE=true
    ROOTFS_ARCHIVE_COMPRESS_DISABLED=true
    citbx_export TARGET_DOCKER_BUILD_FORCE ROOTFS_ARCHIVE_COMPRESS_DISABLED
}

target_build() {
    if [ -f "targets/$TARGET_NAME/build.env" ]; then
        . targets/$TARGET_NAME/build.env
    fi
    # Build docker image
    TARGET_DOCKER_BUILD_ARGS+=(-t "$TARGET_CI_REGISTRY_IMAGE_TAG")
    for build_arg in $DOCKER_BUILD_ARG_LIST; do
        TARGET_DOCKER_BUILD_ARGS+=(--build-arg "$build_arg"="${!build_arg}")
    done
    docker build "${TARGET_DOCKER_BUILD_ARGS[@]}" "targets/$TARGET_NAME/"
    if [ "$TARGET_LATEST_VERSION_ENABLED" == "true" ]; then
        docker tag "$TARGET_CI_REGISTRY_IMAGE_TAG" "${TARGET_CI_REGISTRY_IMAGE_TAG%:*}:latest"
    fi
    if [ -n "$CI_COMMIT_TAG" -o "$TARGET_DOCKER_PUSH_ENABLED" == "true" ] && [ -n "$CI_BUILD_TOKEN" ]; then
        docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
        docker push "$TARGET_CI_REGISTRY_IMAGE_TAG"
        if [ "$TARGET_LATEST_VERSION_ENABLED" == "true" ]; then
            docker push "${TARGET_CI_REGISTRY_IMAGE_TAG%:*}:latest"
        fi
    fi
}

target_export() {
    DOCKER_IMAGE_EXPORT_ID=$(docker run --privileged -d "$TARGET_CI_REGISTRY_IMAGE_TAG" sleep infinity)
    print_note "Extracting rootfs from $TARGET_CI_REGISTRY_IMAGE_TAG docker image..."
    docker exec $DOCKER_IMAGE_EXPORT_ID bash -c '
        touch /tmp/resolv.conf /tmp/hostname /tmp/hosts
        mount --move /etc/resolv.conf /tmp/resolv.conf
        mount --move /etc/hostname /tmp/hostname
        mount --move /etc/hosts /tmp/hosts
        if [ -f /etc/resolv.conf.staging ]; then
            mv /etc/resolv.conf.staging /etc/resolv.conf
        elif [ -x /bin/systemd ]; then
            rm -f /etc/resolv.conf
            ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
        fi
        if [ -f /etc/hostname.staging ]; then
            mv /etc/hostname.staging /etc/hostname
        else
            echo "localhost" > /etc/hostname
        fi
        if [ -f /etc/hosts.staging ]; then
            mv /etc/hosts.staging /etc/hosts
        else
            printf "%s\t%s\n"                               \
                127.0.0.1   localhost                       \
                127.0.1.1   "$(cat /etc/hostname)"          \
                ::1         "ip6-localhost ip6-loopback"    \
                fe00::0     ip6-localnet                    \
                ff00::0     ip6-mcastprefix                 \
                ff02::1     ip6-allnodes                    \
                ff02::2     ip6-allrouters                  \
                > /etc/hosts
        fi
    '
    local dest_dir="artifacts/targets/$TARGET_NAME/$TARGET_VERSION"
    mkdir -p "$dest_dir"
    docker export $DOCKER_IMAGE_EXPORT_ID > "$dest_dir/rootfs.tar"
    if [ "$ROOTFS_ARCHIVE_COMPRESS_DISABLED" != "true" ]; then
        print_note "Compressing the rootfs archive..."
        xz -T0 -c9 "$dest_dir/rootfs.tar" > "$dest_dir/rootfs.tar.xz"
        rm "$dest_dir/rootfs.tar"
    fi
}

job_main() {
    if [ -n "$CI_JOB_TOKEN" ]; then
        # Login only if runned in DIND mode (e.g. on a Gitlab runner and not on a workstation)
        docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
    fi
    if [ "$TARGET_DOCKER_BUILD_FORCE" != "true" ]; then
        # Trying to pull the image if exists
        if docker pull "$TARGET_CI_REGISTRY_IMAGE_TAG"; then
            TARGET_DOCKER_ALREADY_BUILDED=true
            print_note "Target image already builded, skipping build process..."
        fi
    fi
    if [ "$TARGET_DOCKER_ALREADY_BUILDED" != "true" ]; then
        target_build
    fi
    if [ "$TARGET_DOCKER_EXPORT_ENABLED" == "true" ]; then
        target_export
    fi
}

job_after() {
    local retcode=$1
    if [ "$TARGET_DOCKER_ALREADY_BUILDED" != "true" ]; then
        if [ $retcode -eq 0 ]; then
            print_info "Image [$TARGET_CI_REGISTRY_IMAGE_TAG] successfully built"
        fi
    fi
    if [ "$TARGET_DOCKER_EXPORT_ENABLED" == "true" ]; then
        docker rm -f $DOCKER_IMAGE_EXPORT_ID || true
        sudo rm -rf build
        if [ $retcode -eq 0 ]; then
            print_info "Image [$TARGET_NAME/$TARGET_VERSION] successfully extracted"
        fi
    fi
}
