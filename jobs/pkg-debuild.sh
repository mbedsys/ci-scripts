
job_main() {
    if [ -n "$(git diff HEAD)" ]; then
        print_critical "This job cannot be run with unstaged changes!" \
                        " => Please commit your changes firt before run this job again"
    fi
    . /etc/os-release
    : ${OS_VERSION_CODENAME:=$VERSION_CODENAME}
    if [ -z "$PACKAGE_VERSION" ]; then
        print_critical "Undefined VERSION!" "=> Possible reasons:" \
            " * debian.sh file not added into \"$CITBX_ENV_DIR\" directory" \
            " * no version found in debian/changelog file"
    fi
    if [ -n "$CI_COMMIT_TAG" ]; then
        if [[ $CI_COMMIT_TAG != *$PACKAGE_VERSION* ]]; then
            print_critical "The GIT tag [$CI_COMMIT_TAG] seems to be unaligned with package version [$PACKAGE_VERSION]"
        fi
    elif [ "$PACKAGE_VERSION_GIT_SUFFFIX_DISABLE" != "true" ]; then
        PACKAGE_VERSION_TESTING="$PACKAGE_VERSION+git${CI_COMMIT_TIMESTAMP:0:4}${CI_COMMIT_TIMESTAMP:5:2}${CI_COMMIT_TIMESTAMP:8:2}.$CI_COMMIT_SHORT_SHA"
    fi
    local build_dir="${PACKAGE_BUILD_DIR:-build}"
    
    # Setup build directory
    rm -rf "$build_dir" && mkdir -p "$build_dir/${PACKAGE_SOURCE_NAME}/${PACKAGE_PROJECT_PATH}"

    # Generate source folder
    git archive $(git rev-parse HEAD) | tar -C "$build_dir/${PACKAGE_SOURCE_NAME}/" -x

    # Update debian/control with CI environment content
    mv "$build_dir/${PACKAGE_SOURCE_NAME}/debian/control" "$build_dir/${PACKAGE_SOURCE_NAME}/debian/control.orig"
    awk '{
            while(match($0,"[$]{(CI_|PKG_)[^}]*}")) {
                var=substr($0,RSTART+2,RLENGTH -3);
                gsub("[$]{"var"}",ENVIRON[var])
            }
        }1' "$build_dir/${PACKAGE_SOURCE_NAME}/debian/control.orig" > "$build_dir/${PACKAGE_SOURCE_NAME}/debian/control"

    # Generate the source archive
    tar -C "$build_dir/${PACKAGE_SOURCE_NAME}/${PACKAGE_PROJECT_PATH}/" --exclude='./debian' -cJf \
        "$build_dir/${PACKAGE_SOURCE_NAME}/${PACKAGE_PROJECT_PATH}/../${PACKAGE_SOURCE_NAME}_${PACKAGE_VERSION%%-*}.orig.tar.xz" .
    
    # Build the package
    export DEB_BUILD_OPTIONS="parallel=$(nproc)"
    cd "$build_dir/${PACKAGE_SOURCE_NAME}/${PACKAGE_PROJECT_PATH}/"
    if [ -n "$PACKAGE_VERSION_TESTING" ]; then
        dch -v "$PACKAGE_VERSION_TESTING" "Internal release: for testing purpose only!"
    fi
    debuild --preserve-env -us -uc -d
    cd -
    
    # Move .deb files to artifacts folder
    mkdir -p artifacts/packages/$OS_VERSION_CODENAME
    mv "$build_dir/${PACKAGE_SOURCE_NAME}/${PACKAGE_PROJECT_PATH}/../"*.deb artifacts/packages/$OS_VERSION_CODENAME/
    rm -rf "$build_dir"
    find "artifacts/packages/$OS_VERSION_CODENAME" -name "*.deb" -exec \
        bash -e "$CITBX_SCRIPTS_REAL_PATH/tools/build-package-metadata.sh" '{}' \;
    local pkg_list=($(find artifacts/packages/$OS_VERSION_CODENAME/ -name "*.deb" -printf "%f\n"));
    print_info "Package(s) [${pkg_list[*]}]" \
        "  successfully generated into artifacts/packages/$OS_VERSION_CODENAME directory"
}
