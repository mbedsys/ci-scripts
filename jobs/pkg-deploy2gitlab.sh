
job_main() {
    local project_id="$(jq -rn --arg x "$CI_PROJECT_PATH" '$x|@uri')"
    local error
    local links=()
    while read -r metafile; do
        print_note "Reading metadata file [$metafile]..."
        eval "$(cat "$metafile")" || print_critical "Invalid metadata file!"
        for var in PACKAGE_NAME PACKAGE_VERSION PACKAGE_TYPE; do
            test -n "${!var}" || print_critical "Undefined variable $var"
        done
        local srcfile=${metafile%.pkginfo}
        local dstfile="$PACKAGE_NAME/$PACKAGE_VERSION/$(
            sed 's/\s\s*/_/g; s/_$//g;' <<< "$PACKAGE_NAME $PACKAGE_OS_NAME $PACKAGE_OS_VERSION $PACKAGE_ARCH"
                )-$PACKAGE_VERSION.$PACKAGE_TYPE"
        print_note "Deploying package [$dstfile]..."
        eval "$(curl -s --header "JOB-TOKEN: $CI_JOB_TOKEN" \
            --upload-file "$srcfile" \
            "$CI_API_V4_URL/projects/$project_id/packages/generic/$dstfile" | jq -r '"
                error=\(@sh"\(.error // "")")
                message=\(@sh"\(.message // "")")
            "')"
        if [ -n "$error" ] || [ "$message" != "201 Created" ]; then
            print_critical "Fail to deploy package: $error$message"
        fi
        links+=("$(jq -rn \
            --arg name "$PACKAGE_NAME $PACKAGE_TYPE package for $PACKAGE_OS_NAME $PACKAGE_OS_VERSION [$PACKAGE_ARCH]" \
            --arg url "$CI_API_V4_URL/projects/$project_id/packages/generic/$dstfile" \
            '{"name": $name, "url": $url, "link_type": "package"}')")
        print_info "Package [$dstfile] successfully deployed"
    done <<< "$(find artifacts -type f -name "*.pkginfo")"
    if [ -n "$CI_COMMIT_TAG" ] && [ "$GITLAB_RELEASE" != "disabled" ]; then
        : ${RELEASE_DESCRIPTION:="$(
            echo "Change list from the previous version:"
            git log --no-merges --reverse --pretty=format:%s $(
                git describe --tags --abbrev=0 HEAD~1 2>/dev/null || git rev-list --max-parents=0 HEAD
            )..HEAD | sed 's/^/* /g' | grep ''
        )"}
        curl --header 'Content-Type: application/json' --header "JOB-TOKEN: $CI_JOB_TOKEN" --data "$(
            jq -rn --arg tag "$CI_COMMIT_TAG" --arg desc "$RELEASE_DESCRIPTION" '{
            "name": "Version \($tag)",
            "tag_name": $tag,
            "description": $desc,
            "assets": { "links": ['"$(printf ",%s" "${links[@]}" | sed -e '1s/^.//')"'] }
        }')" --request POST "$CI_API_V4_URL/projects/$project_id/releases"
    fi
}
