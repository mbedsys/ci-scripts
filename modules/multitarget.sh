

citbx_module_multitarget_define() {
     bashopts_declare -n TARGET_REF_NAME -l target-ref-name -t string \
        -d "Overload the commit ref name (by default, it's the local branch name)"
}

citbx_module_multitarget_setup() {
    if [ -n "$TARGET_REF_NAME" ]; then
        CI_COMMIT_REF_NAME="$TARGET_REF_NAME"
    fi
}

citbx_module_multitarget_before() {
    TARGET_REF_PARTS=(${CI_COMMIT_REF_NAME//\// })
    TARGET_NAME=${CI_COMMIT_REF_NAME%/*}
    TARGET_VERSION=${CI_COMMIT_REF_NAME##*/}
    TARGET_CI_REGISTRY_IMAGE_TAG="$CI_REGISTRY_IMAGE/$TARGET_NAME:$TARGET_VERSION"
    pattern='^'"$TARGET_NAME"'/.*$'
    if ! [[ $CI_COMMIT_REF_NAME =~ $pattern ]]; then
        print_critical 'This job cannot be launched with the following tag/branch "'"$CI_COMMIT_REF_NAME"'"' \
                        '=> The branch/tag must follow the format '"$TARGET_NAME"'/image_version"'
    fi
    if [ -f "targets/$TARGET_NAME/jobs.env" ]; then
        . "targets/$TARGET_NAME/jobs.env"
    fi
}
