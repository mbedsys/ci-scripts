#!/usr/bin/env bash
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2017-2018 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# Copyright (C) 2019-2021 Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

CITBX_SCRIPTS_DIR=$(readlink -f "$(dirname "$0")")
. "$CITBX_SCRIPTS_DIR/common.sh"

if [ "$GITLAB_CI" != "true" ]; then
    print_critical "This script cannot be launched outside the suitable environment" \
                   "  Please use the command 'ci-toolbox <job_name>' instead" "" \
                   " => To install the ci-toolbox: " \
                   " * https://gitlab.com/mbedsys/citbx4gitlab/blob/master/README.md#ci-toolbox-tool-setup" ""
fi

citbx_env_init
citbx_source_env_dir

# Job end handler
citbx_job_finish() {
    local CITBX_EXIT_CODE=$?
    if [ "$CITBX_JOB_FINISH_CALLED" != "true" ]; then
        CITBX_JOB_FINISH_CALLED="true"
    else
        return 0
    fi
    for hook in $citbx_job_stage_after; do
        $citbx_before_script
        cd $CI_PROJECT_DIR
        $hook $CITBX_EXIT_CODE
        $citbx_after_script
    done
    if [ "$CITBX_EXIT_CODE" == "0" ]; then
        print_info "CI job success!"
    else
        print_error "CI job failure with exit code $CITBX_EXIT_CODE"
    fi
    print_note "Job execution time: $(date +"%H hour(s) %M minute(s) and %S second(s)" -ud @$(($(date +%s) - $CITBX_JOB_START_TIME)))"
}

# Load job
CITBX_JOB_FILE_NAME=${CITBX_JOB_FILE_NAME:-${1:-"$CI_JOB_NAME.sh"}}
module_handler_list="before after"
CITBX_JOB_RUN_FILE_PATH="$CITBX_JOBS_DIR/${CITBX_JOB_FILE_NAME}"
if [ ! -f "$CITBX_JOB_RUN_FILE_PATH" ]; then
    print_critical "Job definition file $CITBX_JOB_RUN_FILE_PATH not found"
fi
. "$CITBX_JOB_RUN_FILE_PATH"
citbx_register_handler "job" "main"
citbx_register_handler "job" "after"
if [ "$CITBX_DEBUG_SCRIPT_ENABLED" == "true" ]; then
    citbx_before_script="set -x"
    citbx_after_script="set +x"
else
    citbx_before_script=""
    citbx_after_script=""
fi
for hook in $citbx_job_stage_before; do
    $citbx_before_script
    cd $CI_PROJECT_DIR
    $hook
    $citbx_after_script
done
CITBX_JOB_START_TIME=$(date +%s)
trap citbx_job_finish EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM
print_info "CI job begin"
if [ -z "$citbx_job_stage_main" ]; then
    print_critical "Function job_main not found in the file $CITBX_JOB_RUN_FILE_PATH"
fi
for hook in $citbx_job_stage_main; do
    $citbx_before_script
    cd $CI_PROJECT_DIR
    $hook
    $citbx_after_script
done
