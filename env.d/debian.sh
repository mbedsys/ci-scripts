
: ${PACKAGE_PROJECT_PATH:="."}
: ${PACKAGE_SOURCE_NAME:="$(awk '/^Source: / {print $2; exit}' ${PACKAGE_PROJECT_PATH}/debian/control \
                                2>/dev/null || true )"}
: ${PACKAGE_VERSION=$(sed 's/^'"${PACKAGE_SOURCE_NAME}"'\s*(\([0-9\.]*\).*$/\1/;t;d' \
                            ${PACKAGE_PROJECT_PATH}/debian/changelog 2>/dev/null | head -n 1 || true )}
