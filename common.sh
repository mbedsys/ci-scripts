#!/usr/bin/env bash
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2017-2018 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# Copyright (C) 2018-2021 Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -eo pipefail

# Print an error message and exit with error status 1
print_critical() {
    >&2 printf "\033[91m[CRIT] %s\033[0m\n" "$@"
    exit 1
}

# Print an error message
print_error() {
    >&2 printf "\033[91m[ERRO] %s\033[0m\n" "$@"
}

# Print a warning message
print_warning() {
    >&2 printf "\033[93m[WARN] %s\033[0m\n" "$@"
}

# Print a note message
print_note() {
    printf "[NOTE] %s\n" "$@"
}

# Pring an info message
print_info() {
    printf "\033[92m[INFO] %s\033[0m\n" "$@"
}

# Get the real core number
ncore() {
    lscpu | awk -F ':' '
        /^Core\(s\) per socket/ {
            nc=$2;
        }
        /^Socket\(s\)/ {
            ns=$2;
        }
        END {
            print nc*ns;
        }'
}

citbx_project_path_reformat() {
    local path=${1:-$2}
    echo "$CI_PROJECT_DIR/${path#$CI_PROJECT_DIR/}"
}

citbx_env_init() {
    # Check bash
    if [ ! "${BASH_VERSINFO[0]}" -ge 4 ]; then
        print_critical "This script needs BASH version 4 or greater"
    fi

    if [ -z "$CI_PROJECT_DIR" ]; then
        # Find the project directory
        CI_PROJECT_DIR="$(git rev-parse --show-toplevel 2>/dev/null || true)"
    fi
    if [ -z "$CI_PROJECT_DIR" ]; then
        return 0
    fi
    if [ -f "$CI_PROJECT_DIR/.ci-toolbox.properties" ]; then
        . "$CI_PROJECT_DIR/.ci-toolbox.properties"
    fi
    CITBX_SCRIPTS_DIR="$(
        citbx_project_path_reformat "$CITBX_SCRIPTS_DIR" ci/scripts)"
    CITBX_SCRIPTS_REAL_PATH="$(dirname "$(readlink -f "$0")")"
    CITBX_MODULES_DIR="$(
        citbx_project_path_reformat "$CITBX_MODULES_DIR" "$CITBX_SCRIPTS_DIR/modules")"
    CITBX_JOBS_DIR="$(
        citbx_project_path_reformat "$CITBX_JOBS_DIR" "$CITBX_SCRIPTS_DIR/jobs")"
    CITBX_ENV_DIR="$(
        citbx_project_path_reformat "$CITBX_ENV_DIR" "$CITBX_SCRIPTS_DIR/env.d")"
}

citbx_source_env_dir() {
    if [ -d "$CITBX_ENV_DIR" ]; then
        for f in $CITBX_ENV_DIR/*.sh; do
            source "$f"
        done
    fi
}

citbx_register_handler() {
    local list="citbx_job_stage_${2}"
    local func="${1}_${2}"
    if [[ "$(type -t $func)" != "function" ]]; then
        return 0
    fi
    local pattern='\b'"$func"'\b'
    if [[ "${!list}" =~ $pattern ]]; then
        return 0
    fi
    case "$2" in
        define|setup|before|main)
            eval "${list}=\"${!list} $func\""
            ;;
        after)
            eval "${list}=\"$func ${!list}\""
            ;;
        *)
            print_critical "Use: citbx_register_handler <prefix> define|setup|before|main|after"
            ;;
    esac
}

declare -A CITBX_USE_LIST
# Add module in the use list
citbx_use() {
    local module=$1
    if [ -z "$module" ]; then
        print_critical "Usage: citbx_use <module_name>"
    fi
    if [ "${CITBX_USE_LIST[$module]}" == "true" ]; then
        return 0
    fi
    if [ ! -f "$CITBX_MODULES_DIR/${module}.sh" ]; then
        print_critical "Module ${module} not found!"
    fi
    . "$CITBX_MODULES_DIR/${module}.sh"
    CITBX_USE_LIST[$module]="true"
    for h in $module_handler_list; do
        citbx_register_handler "citbx_module_${module}" $h
    done
}
